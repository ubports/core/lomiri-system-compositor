/*
 * Copyright © 2015 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Andreas Pokorny <andreas.pokorny@canonical.com>
 */

#ifndef LSC_DBUS_CONNECTION_THREAD_H_
#define LSC_DBUS_CONNECTION_THREAD_H_

#include <memory>
#include <thread>

namespace lsc
{

class DBusEventLoop;
class DBusConnectionThread
{
public:
    DBusConnectionThread(std::shared_ptr<DBusEventLoop> const& thread);
    ~DBusConnectionThread();
    DBusEventLoop & loop();

private:
    std::shared_ptr<DBusEventLoop> dbus_event_loop;
    std::thread dbus_loop_thread;
};

}

#endif
